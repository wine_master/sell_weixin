package com.sell.service;

import com.sell.SellBackApplication;
import com.sell.entity.OrderDetail;
import com.sell.mapper.OrderDetailMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * @Company
 * @Classname TestOrderDetailService
 * @Description TODO
 * @Author pn
 * Date 2021-09-07 17:26
 * Version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SellBackApplication.class)
public class TestOrderDetailService {
    @Autowired
    private OrderDetailService orderDetailService;

    @Test
    public void testFindOrderDetailsbyorderid(){
        try {
            List<OrderDetail> orderDetails = orderDetailService.findOrderDetailsbyorderid("c40f20210822121737627order");
            System.out.println(orderDetails);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
