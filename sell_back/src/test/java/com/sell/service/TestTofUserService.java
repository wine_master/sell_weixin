package com.sell.service;

import com.sell.SellBackApplication;
import com.sell.entity.TofUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @Company
 * @Classname TestTofUserService
 * @Description TODO
 * @Author pn
 * Date 2021-09-07 17:14
 * Version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SellBackApplication.class)
public class TestTofUserService {
    @Autowired
    private TofUserService tofUserService;

    @Test
    public void testGetTofUserByopenid(){
        try {
            TofUser user = tofUserService.getTofUserByopenid("oZkCm5rOCILw8EApW6in5X8-AFqg");
            System.out.println(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
