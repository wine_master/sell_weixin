package com.sell.service;

import com.sell.SellBackApplication;
import com.sell.entity.ProductInfo;
import com.sell.entity.ProductType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @Company
 * @Classname TestProductTypeService
 * @Description TODO
 * @Author pn
 * Date 2021-09-07 9:55
 * Version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SellBackApplication.class)
public class TestProductInfoService {
    @Autowired
    private  ProductInfoService productInfoService;
    @Test
    public void testFindProductInfoById(){
        try {
            ProductInfo productInfo = productInfoService.findproductinfobyid(1);
            System.out.println(productInfo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
