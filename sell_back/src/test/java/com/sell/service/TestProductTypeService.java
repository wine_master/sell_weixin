package com.sell.service;

import com.sell.SellBackApplication;
import com.sell.entity.ProductType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @Company
 * @Classname TestProductTypeService
 * @Description TODO
 * @Author pn
 * Date 2021-09-07 9:55
 * Version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SellBackApplication.class)
public class TestProductTypeService {
    @Autowired
    private ProductTypeService productTypeService;
    @Test
    public void testFindProductTypeById(){
        try {
            ProductType productType = productTypeService.findproducttypebyid(1);

            //断言
            Assert.assertEquals("图书",productType.getProducttypename());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
