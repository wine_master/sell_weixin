package com.sell.service;

import com.sell.SellBackApplication;
import com.sell.entity.OrderMaster;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @Company
 * @Classname TestOrderMasterService
 * @Description TODO
 * @Author pn
 * Date 2021-09-07 17:19
 * Version 1.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SellBackApplication.class)
public class TestOrderMasterService {
    @Autowired
    private OrderMasterService orderMasterService;

    @Test
    public void testFindOrderMasterbyid(){
        try {
            OrderMaster orderMaster = orderMasterService.findOrderMasterbyid("64fd20210823100419899order");
            System.out.println(orderMaster);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
