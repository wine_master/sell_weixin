package com.sell.mapper;

import com.sell.entity.ProductType;
import com.sell.vo.ProductTypeVo;

import java.util.List;

/**
 * @Company
 * @Interfacename ProductTypeMapper
 * @Description TODO
 * @Author pn
 * Date 2021-09-01 14:31
 * Version 1.0
 */
public interface ProductTypeMapper {
    //新增
    int insertproducttype(ProductType productType) throws Exception;

    //修改
    int updateproducttype(ProductType productType) throws Exception;

    //删除
    int deleteproducttypebyid(int id) throws Exception;

    //根据id查询
    ProductType findproducttypebyid(int id) throws Exception;

    //多条件查询iy
    //List<ProductType> listproducttypebyvo(ProductTypeVo productTypeVo) throws Exception;
}
