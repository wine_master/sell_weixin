package com.sell.mapper;

import com.sell.entity.OrderDetail;

import java.util.List;

/**
 * @Company
 * @Interfacename OrderDetailMapper
 * @Description TODO
 * @Author pn
 * Date 2021-09-01 19:31
 * Version 1.0
 */
public interface OrderDetailMapper {
    //新增清单
    int insertOrderDetail(OrderDetail orderDetail) throws Exception;

    //修改清单
    int updateOrderDetail(OrderDetail orderDetail) throws Exception;

    //根据清单id查询清单
    OrderDetail findOrderDetailbyorderdetailid(String orderdetailid) throws Exception;

    //根据订单id查询清单
    List<OrderDetail> findOrderDetailsbyorderid(String orderid) throws Exception;
}
