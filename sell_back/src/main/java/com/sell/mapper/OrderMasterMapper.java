package com.sell.mapper;

import com.sell.entity.OrderMaster;
import com.sell.vo.OrderMasterVo;

import java.util.List;
import java.util.Map;

/**
 * @Company
 * @Interfacename OrderMasterMapper
 * @Description TODO
 * @Author pn
 * Date 2021-09-01 17:01
 * Version 1.0
 */
public interface OrderMasterMapper {
    //新增订单
    int insertOrderMaster(OrderMaster orderMaster) throws Exception;

    //修改订单及清单
    int updateOrderMaster(OrderMaster orderMaster) throws Exception;

    //改订单支付状态及订单状态
    int updateOrderMasterPayStatus(Map<String,String> map) throws Exception;

    //根据id查询订单
    OrderMaster findOrderMasterbyid(String orderid)throws Exception;

    //多条件查询订单
    List<OrderMaster> listOrderMasterbyVo(OrderMasterVo orderMasterVo);
}
