package com.sell.convert;

import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @Company
 * @Classname DateConverter
 * @Description TODO
 * @Author pn
 * Date 2021-09-07 22:07
 * Version 1.0
 */

//如果是使用@RequestParam注解获取的数据使用如下代码
@Component
public class DateConverter implements Converter<String, Date> {
    private static Logger logger = LoggerFactory.getLogger(DateConverter.class);
    private static final String[] DATE_FORMATTERS = new String[]{
            "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM",
            "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm", "yyyy/MM",
            "yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm", "yyyy.MM"};

    @Override
    public Date convert(String s) {
        try {
            return DateUtils.parseDate(s, DATE_FORMATTERS);
        } catch (java.text.ParseException e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }
}
