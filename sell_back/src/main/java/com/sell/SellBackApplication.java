package com.sell;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Company
 * @Classname SellBackApplication
 * @Description TODO sell_back的启动类
 * @Author pn
 * Date 2021-08-31 22:08
 * Version 1.0
 */
@SpringBootApplication
@MapperScan("com.sell.mapper")
public class SellBackApplication {
    public static void main(String[] args) {
        SpringApplication.run(SellBackApplication.class,args);
    }
}
