package com.sell.service;

import com.sell.entity.ProductType;

/**
 * @Company
 * @Interfacename ProductTypeService
 * @Description TODO
 * @Author pn
 * Date 2021-09-07 9:41
 * Version 1.0
 */
public interface ProductTypeService {
    //新增
    int insertproducttype(ProductType productType) throws Exception;

    //修改
    int updateproducttype(ProductType productType) throws Exception;

    //删除
    int deleteproducttypebyid(int id) throws Exception;

    //根据id查询
    ProductType findproducttypebyid(int id) throws Exception;
}
