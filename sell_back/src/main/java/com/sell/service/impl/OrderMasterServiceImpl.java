package com.sell.service.impl;

import com.sell.entity.OrderDetail;
import com.sell.entity.OrderMaster;
import com.sell.form.OrderForm;
import com.sell.form.OrderOut;
import com.sell.mapper.OrderDetailMapper;
import com.sell.mapper.OrderMasterMapper;
import com.sell.service.OrderMasterService;
import com.sell.utils.IdProduceUtil;
import com.sell.vo.OrderMasterVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Company
 * @Classname OrderMasterServiceImpl
 * @Description TODO
 * @Author pn
 * Date 2021-09-07 16:53
 * Version 1.0
 */
@Service
public class OrderMasterServiceImpl implements OrderMasterService {

    @Resource
    private OrderMasterMapper orderMasterMapper;

    @Resource
    private OrderDetailMapper orderDetailMapper;

    //新增订单
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public OrderOut insertOrderMaster(OrderForm orderForm) throws Exception {
        //订单
        OrderMaster om = new OrderMaster();
        om.setOrderid(IdProduceUtil.produceIdByUUID("order"));//生成订单id
        om.setOpenid(orderForm.getOpenid());//微信id
        om.setBuyeraddress(orderForm.getBuyeraddress());
        om.setBuyername(orderForm.getBuyername());
        om.setBuyertelphone(orderForm.getBuyertelphone());
        om.setCreatetime(new Date());
        om.setOrderstatus(0);  //订单状态
        om.setPaystatus(0);//支付状态 0 未支付 1 已支付 2 已退款
        //订单总价
        double totalaccount = 0d;

        //取得清单(可能1件或多件商品)
        List<OrderDetail> orderDetails = orderForm.getLstOrderDetails();
        for (OrderDetail o : orderDetails) {
            //设置清单号
            o.setOrderdetailid(IdProduceUtil.produceIdByUUID("orderdetail"));//清单id
            o.setOrderid(om.getOrderid());//给清单设置订单号
            o.setCreattime(new Date());
            //计算订单总价
            totalaccount += o.getNum() * o.getPrice();
        }
        //订单总价
        om.setTotalaccount(totalaccount);

        int flag = -1;
        int flag2 = -1;

        //如果库中订单与清单有主外键关系
        //生成订单
        flag = orderMasterMapper.insertOrderMaster(om);


        //生成清单
        for (OrderDetail orderDetail : orderDetails) {
            flag2 = orderDetailMapper.insertOrderDetail(orderDetail);
            if (flag2 <= 0) {
                break;
            }
            //人为抛出一个异常，看能不能事务回滚
           /* int c = 100;
            int d = 0;
            int e = c/d;*/
        }
        OrderOut orderOut=null;
        if(flag > 0 && flag2 > 0){
            orderOut = new OrderOut();
            orderOut.setOrderid(om.getOrderid());
            orderOut.setBuyername(om.getBuyername());
            orderOut.setBuyertelphone(om.getBuyertelphone());
            orderOut.setBuyeraddress(om.getBuyeraddress());
            orderOut.setTotalaccount(om.getTotalaccount());
            orderOut.setCreatetime(om.getCreatetime());
        }
        return orderOut;
    }

    //修改订单及清单
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public int updateOrderMaster(OrderMaster orderMaster, OrderForm orderForm) throws Exception {
        List<OrderDetail> lstOrderDetails = orderForm.getLstOrderDetails();
        for (OrderDetail orderDetail : lstOrderDetails) {
            orderDetailMapper.updateOrderDetail(orderDetail);
        }
        return orderMasterMapper.updateOrderMaster(orderMaster);
    }

    //修改订单状态及相关信息
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public int updateOrderMasterPayStatus(Map<String, String> map) throws Exception {
        return orderMasterMapper.updateOrderMasterPayStatus(map);
    }

    //根据id查询订单
    @Override
    public OrderMaster findOrderMasterbyid(String orderid) throws Exception {
        return orderMasterMapper.findOrderMasterbyid(orderid);
    }

    //多条件查询订单
    @Override
    public List<OrderMaster> listOrderMasterbyVo(OrderMasterVo orderMasterVo) {
        return orderMasterMapper.listOrderMasterbyVo(orderMasterVo);
    }
}
