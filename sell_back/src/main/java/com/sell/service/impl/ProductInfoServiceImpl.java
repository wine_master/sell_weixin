package com.sell.service.impl;

import com.sell.entity.ProductInfo;
import com.sell.mapper.ProductInfoMapper;
import com.sell.service.ProductInfoService;
import com.sell.vo.ProductInfoVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Company
 * @Classname
 * @Description TODO
 * @Author pn
 * Date 2021.9.7 11:00
 * Version 1.0
 */
@Service
public class ProductInfoServiceImpl implements ProductInfoService {
    //注入数据层的Dao
    @Resource
    private ProductInfoMapper productInfoMapper;

    @Override
    public int insertproductinfo(ProductInfo productinfo) throws Exception {
        return productInfoMapper.insertproductinfo(productinfo);
    }

    @Override
    public int updateproductinfo(ProductInfo productinfo) throws Exception {
        return productInfoMapper.updateproductinfo(productinfo);
    }

    @Override
    public int updateproductstatus(ProductInfo productinfo) throws Exception {
        return productInfoMapper.updateproductstatus(productinfo);
    }

    @Override
    public int deleteproductinfo(int id) throws Exception {
        return productInfoMapper.deleteproductinfo(id);
    }

    @Override
    public ProductInfo findproductinfobyid(int id) throws Exception {
        return productInfoMapper.findproductinfobyid(id);
    }

    @Override
    public List<ProductInfo> listproductinfobyvo(ProductInfoVo productInfoVo) throws Exception {
        return productInfoMapper.listproductinfobyvo(productInfoVo);
    }
}
