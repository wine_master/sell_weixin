package com.sell.service.impl;

import com.sell.entity.OrderDetail;
import com.sell.mapper.OrderDetailMapper;
import com.sell.service.OrderDetailService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Company
 * @Classname OrderDetailServiceImpl
 * @Description TODO
 * @Author pn
 * Date 2021-09-07 17:10
 * Version 1.0
 */
@Service
public class OrderDetailServiceImpl implements OrderDetailService {

    @Resource
    private OrderDetailMapper orderDetailMapper;

    //新增清单
    @Override
    public int insertOrderDetail(OrderDetail orderDetail) throws Exception {
        return orderDetailMapper.insertOrderDetail(orderDetail);
    }

    //修改清单
    @Override
    public int updateOrderDetail(OrderDetail orderDetail) throws Exception {
        return orderDetailMapper.updateOrderDetail(orderDetail);
    }

    //根据清单id查询清单
    @Override
    public OrderDetail findOrderDetailbyorderdetailid(String orderdetailid) throws Exception {
        return orderDetailMapper.findOrderDetailbyorderdetailid(orderdetailid);
    }

    //根据订单id查询清单
    @Override
    public List<OrderDetail> findOrderDetailsbyorderid(String orderid) throws Exception {
        return orderDetailMapper.findOrderDetailsbyorderid(orderid);
    }
}
