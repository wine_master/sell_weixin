package com.sell.service.impl;

import com.sell.entity.ProductType;
import com.sell.mapper.ProductTypeMapper;
import com.sell.service.ProductTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Company
 * @Classname ProductTypeServiceImpl
 * @Description TODO
 * @Author pn
 * Date 2021-09-07 9:42
 * Version 1.0
 */

@Service
public class ProductTypeServiceImpl implements ProductTypeService {
    @Resource
    private ProductTypeMapper productTypeMapper;

    @Override
    public int insertproducttype(ProductType productType) throws Exception {
        return productTypeMapper.insertproducttype(productType);
    }

    @Override
    public int updateproducttype(ProductType productType) throws Exception {
        return productTypeMapper.updateproducttype(productType);
    }

    @Override
    public int deleteproducttypebyid(int id) throws Exception {
        return productTypeMapper.deleteproducttypebyid(id);
    }

    @Override
    public ProductType findproducttypebyid(int id) throws Exception {
        return productTypeMapper.findproducttypebyid(id);
    }
}
