package com.sell.service.impl;

import com.sell.entity.TofUser;
import com.sell.mapper.TofUserMapper;
import com.sell.service.TofUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @Company
 * @Classname TofUserServiceImpl
 * @Description TODO
 * @Author pn
 * Date 2021-09-07 16:50
 * Version 1.0
 */
@Service
public class TofUserServiceImpl implements TofUserService {
    @Resource
    private TofUserMapper tofUserMapper;

    @Override
    public TofUser getTofUserByopenid(String openid) throws Exception {
        return tofUserMapper.getTofUserByopenid(openid);
    }

    @Override
    public int insertTofUserByWeixin(TofUser tofUser) throws Exception {
        return tofUserMapper.insertTofUserByWeixin(tofUser);
    }

    @Override
    public int updateTofUserByWeixin(TofUser tofUser) throws Exception {
        return tofUserMapper.updateTofUserByWeixin(tofUser);
    }

    @Override
    public int updateTofUserByAuthorized(Map<String, String> map) throws Exception {
        return tofUserMapper.updateTofUserByAuthorized(map);
    }
}
