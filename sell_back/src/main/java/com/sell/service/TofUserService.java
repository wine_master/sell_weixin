package com.sell.service;

import com.sell.entity.TofUser;

import java.util.Map;

/**
 * @Company
 * @Interfacename TofUserService
 * @Description TODO
 * @Author pn
 * Date 2021-09-07 16:47
 * Version 1.0
 */
public interface TofUserService {
    //微信登录时，根据openid查询用户
    TofUser getTofUserByopenid(String oepnid) throws Exception;

    //如果没有该openid对应的账号信息，则生成新用户的注册信息：userid、昵称、头像、性别、国家、省份、城市
    int insertTofUserByWeixin(TofUser tofUser) throws Exception;

    //同步更新用户信息:昵称、头像、性别、国家、省份、城市
    int updateTofUserByWeixin(TofUser tofUser) throws Exception;

    //微信公众号授权成功后，关联绑定电话、邮箱
    int updateTofUserByAuthorized(Map<String,String> map) throws Exception;
}
