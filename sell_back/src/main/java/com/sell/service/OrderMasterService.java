package com.sell.service;

import com.sell.entity.OrderMaster;
import com.sell.form.OrderForm;
import com.sell.form.OrderOut;
import com.sell.vo.OrderMasterVo;

import java.util.List;
import java.util.Map;

/**
 * @Company
 * @Interfacename OrderMasterService
 * @Description TODO
 * @Author pn
 * Date 2021-09-07 16:48
 * Version 1.0
 */
public interface OrderMasterService {
    //生成订单及清单(事务)
    OrderOut insertOrderMaster(OrderForm orderForm) throws Exception;

    //修改订单及清单
    int updateOrderMaster(OrderMaster orderMaster, OrderForm orderForm) throws Exception;

    //修改订单支付状态
    int updateOrderMasterPayStatus(Map<String,String> map) throws Exception;

    //根据id查询订单
    OrderMaster findOrderMasterbyid(String orderid) throws Exception;

    //多条件查询订单
    List<OrderMaster> listOrderMasterbyVo(OrderMasterVo orderMasterVo);
}
