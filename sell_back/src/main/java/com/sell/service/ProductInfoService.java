package com.sell.service;

import com.sell.entity.ProductInfo;
import com.sell.vo.ProductInfoVo;

import java.util.List;

/**
 * @Company
 * @Interfacename ProductInfoService
 * @Description TODO
 * @Author pn
 * Date 2021-09-07 15:48
 * Version 1.0
 */
public interface ProductInfoService {
    //新增
    int insertproductinfo(ProductInfo productinfo) throws Exception;

    //修改
    int updateproductinfo(ProductInfo productinfo) throws Exception;

    //改状态
    int updateproductstatus(ProductInfo productinfo) throws Exception;

    //删除
    int deleteproductinfo(int id) throws Exception;

    //根据id查询
    ProductInfo findproductinfobyid(int id) throws Exception;

    //多条件查询
    List<ProductInfo> listproductinfobyvo(ProductInfoVo productInfoVo) throws Exception;
}
