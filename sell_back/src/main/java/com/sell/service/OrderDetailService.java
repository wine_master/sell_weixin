package com.sell.service;

import com.sell.entity.OrderDetail;

import java.util.List;

/**
 * @Company
 * @Interfacename OrderDetailService
 * @Description TODO
 * @Author pn
 * Date 2021-09-07 16:49
 * Version 1.0
 */
public interface OrderDetailService {
    //新增清单
    int insertOrderDetail(OrderDetail orderDetail) throws Exception;

    //修改清单
    int updateOrderDetail(OrderDetail orderDetail) throws Exception;

    //根据清单id查询清单
    OrderDetail findOrderDetailbyorderdetailid(String orderdetailid) throws Exception;

    //根据订单id查询清单
    List<OrderDetail> findOrderDetailsbyorderid(String orderid) throws Exception;
}
