package com.sell.controller;

import com.sell.entity.TofUser;
import com.sell.result.ResultStatus;
import com.sell.service.TofUserService;
import com.sell.utils.IdProduceUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Company
 * @Classname TofUserController
 * @Description TODO
 * @Author pn
 * Date 2021-09-07 22:15
 * Version 1.0
 */
@RestController
@RequestMapping("/tofuser/*")
public class TofUserController {
    @Autowired
    private TofUserService tofUserService;

    //微信登录时，根据openid查询用户
    @PostMapping("getTofUserByopenid")
    public TofUser getTofUserByopenid(@RequestBody Map<String, String> map) {
        try {
            return tofUserService.getTofUserByopenid(map.get("openid"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
        //localhost:9091/tofuser/getTofUserByopenid
    }

    //如果没有该openid对应的账号信息，则生成新用户的注册信息：userid、昵称、头像、性别、国家、省份、城市
    @PostMapping("insertTofUserByWeixin")
    public String insertTofUserByWeixin(@RequestBody TofUser tofUser) {
        int rows = 0;
        try {
            //生成userid
            tofUser.setUserid(IdProduceUtil.produceIdByUUID("user"));
            rows = tofUserService.insertTofUserByWeixin(tofUser);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //注意返回自动序列化成json格式
        return rows > 0 ? ResultStatus.SUCCESS.toString(): ResultStatus.FAIL.toString();
        //localhost:9091/tofuser/insertTofUserByWeixin
    }

    //同步更新用户信息:昵称、头像、性别、国家、省份、城市
    @PostMapping("updateTofUserByWeixin")
    public String updateTofUserByWeixin(@RequestBody TofUser tofUser){
        int rows = 0;
        try {
            rows = tofUserService.updateTofUserByWeixin(tofUser);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rows > 0 ? ResultStatus.SUCCESS.toString(): ResultStatus.FAIL.toString();
    }

    //微信公众号授权成功后，关联绑定电话、邮箱
    @PostMapping("updateTofUserByAuthorized")
    public String updateTofUserByAuthorized(@RequestBody Map<String,String> map){//【注意】客户端传的是map的json串，此处用Map接收入参，自动完成转换
        int rows = 0;
        try {
            rows = tofUserService.updateTofUserByAuthorized(map);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rows > 0 ? ResultStatus.SUCCESS.toString(): ResultStatus.FAIL.toString();
        //localhost:9091/tofuser/updateTofUserByAuthorized
    }
}
