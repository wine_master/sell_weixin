package com.sell.controller;

import com.sell.entity.ProductInfo;
import com.sell.result.ResultStatus;
import com.sell.service.ProductInfoService;
import com.sell.vo.ProductInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Company
 * @Classname ProductInfoController
 * @Description TODO
 * @Author pn
 * Date 2021-09-07 22:45
 * Version 1.0
 */
@RestController
@RequestMapping("/product/*")
public class ProductInfoController {
    @Autowired
    private ProductInfoService productInfoService;

    //取得所有商品
    @GetMapping("list")
    public List<ProductInfo> getall() {
        ProductInfoVo productInfoVo = new ProductInfoVo();
        try {
            return productInfoService.listproductinfobyvo(productInfoVo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
        //localhost:80/product/list
    }

    //查询商品
    @GetMapping("findproductbyid")
    public ProductInfo findProductInfobyid(@RequestParam(value = "productid") int productid) {
        try {
            return productInfoService.findproductinfobyid(productid);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
        //localhost:80/product/findproductbyid?productid=1
    }

    //查询商品，传参用Restful风格定义
    /*@GetMapping("findproductbyid/{productid}")
    public ProductInfo findProductInfobyid(@PathVariable(value = "productid") int productid){
        //客户端调用应该这样传参
        //localhost:80/product/findproductbyid/1
    }*/

    //删除商品
    //localhost:80/product/deleteproductinfo?productid=1
    @GetMapping("deleteproductinfo")
    public String deleteProductInfo(@RequestParam(value = "productid") int productid) {
        int rows = 0;
        try {
            rows = productInfoService.deleteproductinfo(productid);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rows > 0 ? ResultStatus.SUCCESS.toString() : ResultStatus.FAIL.toString();
    }

    //改状态
    //localhost:80/product/updateproductstatus?
    @PostMapping("updateproductstatus")
    public String updateProductStatus(@RequestBody ProductInfo productInfo) {
        int rows = 0;
        try {
            rows = productInfoService.updateproductstatus(productInfo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rows > 0 ? ResultStatus.SUCCESS.toString() : ResultStatus.FAIL.toString();
    }

    //修改
    //localhost:80/product/updateproductinfo?
    @PostMapping("updateproductinfo")
    public String updateProductInfo(@RequestBody ProductInfo productInfo){
        int rows = 0;
        try {
            rows = productInfoService.updateproductinfo(productInfo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rows > 0?ResultStatus.SUCCESS.toString():ResultStatus.FAIL.toString();
    }


    //新增
    //localhost:80/product/insertproductinfo?
    @PostMapping("insertproductinfo")
    public String insertProductInfo(@RequestBody ProductInfo productInfo){
        int rows = 0;
        try {
            rows = productInfoService.insertproductinfo(productInfo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rows > 0? ResultStatus.SUCCESS.toString():ResultStatus.FAIL.toString();
    }
}
