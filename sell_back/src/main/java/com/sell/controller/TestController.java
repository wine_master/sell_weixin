package com.sell.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Company
 * @Classname TestController
 * @Description TODO
 * @Author pn
 * Date 2021-08-31 22:44
 * Version 1.0
 */
@RestController
public class TestController {
    @RequestMapping("index")
    public String test(){
        return "hello world";
    }
}
