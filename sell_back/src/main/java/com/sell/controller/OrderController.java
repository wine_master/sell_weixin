package com.sell.controller;

import com.alibaba.fastjson.JSON;
import com.sell.entity.OrderMaster;
import com.sell.form.OrderForm;
import com.sell.form.OrderForm2;
import com.sell.form.OrderOut;
import com.sell.result.OrderResult;
import com.sell.result.ResultStatus;
import com.sell.service.OrderMasterService;
import com.sell.vo.OrderMasterCondition;
import com.sell.vo.OrderMasterVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Company
 * @Classname OrderController
 * @Description TODO
 * @Author pn
 * Date 2021-09-08 12:33
 * Version 1.0
 */
@RestController
@RequestMapping("/order/*")
public class OrderController {
    @Autowired
    private OrderMasterService orderMasterService;

    //生成订单: @requestBody注解需要在post请求下才能正常使用.
    @PostMapping("createOrder")//接收客户端json数据，用@RequestBody
    public OrderResult createOrder(@RequestBody OrderForm orderForm) {
        OrderOut orderOut = null;
        try {
            orderOut = orderMasterService.insertOrderMaster(orderForm);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //返回结果
        OrderResult orderResult = new OrderResult();
        //封装返回数据
        orderResult.setData(JSON.toJSONString(orderOut));
        orderResult.setMessge(ResultStatus.SUCCESS.toString());
        return orderResult;
    }

    //查询
    @PostMapping("queryOrderbyid")
    public OrderMaster queryOrderbyid(@RequestBody Map<String, String> map) {
        try {
            return orderMasterService.findOrderMasterbyid(map.get("orderid"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //修改订单及清单
    @PostMapping("updateOrder")
    public String updateOrder(@RequestBody OrderMaster orderMaster,@RequestBody OrderForm orderForm){
        int rows = 0;
        try {
            rows = orderMasterService.updateOrderMaster(orderMaster, orderForm);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rows > 0?ResultStatus.SUCCESS.toString():ResultStatus.FAIL.toString();
    }
    //改支付状态 订单状态 减库存等
    @PostMapping("upateOrderPayStatus")
    public String updateOrderMasterPayStatus(@RequestBody Map<String, String> map) {
        //取得参数
      /*  String orderid = map.get("orderid");
        int orderstatus = Integer.valueOf(map.get("orderstatus")); //订单状态： 0 等待买家付款 1 买家已付款,等待发货 2 卖家已发货 3交易成功 4交易关闭
        int paystatus = Integer.valueOf(map.get("paystatus"));//支付状态： 0 待买家支付 1 买家已支付 2 已退款 3订单已关闭
        String transactionid = map.get("transactionid");//微信支付订单号*/


        int rows = -1;
        try {
            //更新时间
            map.put("updatetime", String.valueOf(new Date()));
            rows = orderMasterService.updateOrderMasterPayStatus(map);
            return (rows > 0) ? ResultStatus.SUCCESS.toString() : ResultStatus.FAIL.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResultStatus.FAIL.toString();
    }


    //前台: 查询买家个人订单
    @PostMapping("listorderbybuyer")
    public List<OrderMaster> listOrderMaster(@RequestBody OrderForm2 orderForm2) {
        OrderMasterVo omv = new OrderMasterVo();
        OrderMasterCondition omc = new OrderMasterCondition();

        if (null != orderForm2) {
            //赋值
            omc.setOpenid(orderForm2.getOpenid());//用户标识
            omc.setOrderid(orderForm2.getOrderid());
            omc.setBuyername(orderForm2.getBuyername());
            omc.setBuyertelphone(orderForm2.getBuyertelphone());
            omc.setOrderstatus(orderForm2.getOrderstatus());//订单状态： 0 等待买家付款 1 买家已付款,等待发货 2 卖家已发货 3交易成功 4交易关闭
            omc.setCreatetime(orderForm2.getCreatetime());//下单时间
            omv.setOrderMasterCondition(omc);
        }
        List<OrderMaster> orderMasters = orderMasterService.listOrderMasterbyVo(omv);
        return orderMasters;
    }
}
