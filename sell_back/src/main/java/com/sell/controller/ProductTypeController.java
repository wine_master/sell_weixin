package com.sell.controller;

import com.sell.entity.ProductType;
import com.sell.result.ResultStatus;
import com.sell.service.ProductTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Company
 * @Classname ProductTypeController
 * @Description TODO
 * @Author pn
 * Date 2021-09-08 13:22
 * Version 1.0
 */
@RestController
@RequestMapping("/productType/*")
public class ProductTypeController {
    @Autowired
    private ProductTypeService productTypeService;

    //新增商品类型
    @PostMapping("insertproducttype")
    public String insertProductType(@RequestBody ProductType productType) {
        int rows = 0;
        try {
            rows = productTypeService.insertproducttype(productType);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rows > 0 ? ResultStatus.SUCCESS.toString() : ResultStatus.FAIL.toString();
    }

    //修改商品类目
    @PostMapping("updateproducttype")
    public String updateProductType(@RequestBody ProductType productType) {
        int rows = 0;
        try {
            rows = productTypeService.updateproducttype(productType);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rows > 0 ? ResultStatus.SUCCESS.toString() : ResultStatus.FAIL.toString();
    }

    //删除商品类目
    @GetMapping("deleteproducttypebyid")
    public String deleteProductTypeById(@RequestParam(value = "id") int id) {
        int rows = 0;
        try {
            rows = productTypeService.deleteproducttypebyid(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rows > 0?ResultStatus.SUCCESS.toString():ResultStatus.FAIL.toString();
    }

    //根据id查询商品类目信息
    @GetMapping("findproducttypebyid")
    public ProductType findProductTypeById(@RequestParam(value = "id") int id){
        try {
            return productTypeService.findproducttypebyid(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
