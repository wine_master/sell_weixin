package com.sell.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;


/**
 * @Company
 * @Classname IdProudceUtil
 * @Description TODO 生成业务id工具类：分布式用Zookeeper分布式ID工具生成
 * @Author pn
 * Date 2021-09-07 16:59
 * Version 1.0
 */
public class IdProduceUtil {
    /**
     * @methodName TODO produceIdByUUID
     *
     * @param businessType 业务类型，比如:order 表示订单  user表示用户
     * @author Administrator
     * @date 2021.7.24 21:43
     * @return java.lang.String
     */
    public static String produceIdByUUID(String businessType){
        //商户系统内部订单号，要求32个字符内，只能是数字、大小写字母_-|* 且在同一个商户号下唯一。
        String random = UUID.randomUUID().toString().substring(1,5);
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmssSSS");
        String datestring = dateFormat.format(new Date());
        return random+datestring+businessType;
    }

    //测试
    public static void main(String[] args) {
        String result = produceIdByUUID("order");
        System.out.println(result+ "  length:  "+result.length());
    }
}
