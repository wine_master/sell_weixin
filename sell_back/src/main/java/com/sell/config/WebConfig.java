package com.sell.config;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.sell.convert.DateConverter;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * @Company
 * @Classname WebConfig
 * @Description TODO 扩展原spring-mvc 的相关配置,相当于在spring mvc时在 application.xml文件中注册一个bean
 * @Author pn
 * Date 2021-09-07 21:57
 * Version 1.0
 */

@Configuration
//@EnableWebMvc 这个注解会让springboot的所有默认配置失效，所以不要加，否则就跟手动写spring-mvc项目一样了。
public class WebConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //限流拦截器:限流（每秒处理量）
        //registry.addInterceptor(new LimitInterceptor(1000, LimitInterceptor.LimitType.WAIT)).addPathPatterns("/**");
        //WebMvcConfigurer.super.addInterceptors(registry);
    }

    //SpringBoot全局配置日期类型参数Date自动绑定，返回结果自动格式化
    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new DateConverter());
    }

    //SpringBoot跨域问题CORS解决
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        String mapping = "/**"; // 所有请求，也可配置成特定请求，如/api/**
        String origins = "*"; // 所有来源，也可以配置成特定的来源才允许跨域，如http://www.xxxx.com
        String methods = "*"; // 所有方法，GET、POST、PUT等
        registry.addMapping(mapping).allowedOrigins(origins).allowedMethods(methods);
    }

    //定义消息转换器，使用FastJson替换jackson
    @Bean
    public HttpMessageConverters fastjsonHttpMessageConverter(){
        //定义消息转换对象
        FastJsonHttpMessageConverter fastConverter
                =new FastJsonHttpMessageConverter();

        FastJsonConfig fastJsonConfig = new FastJsonConfig();
        fastJsonConfig.setSerializerFeatures(SerializerFeature.PrettyFormat);

        //在转换器中添加配置信息
        fastConverter.setFastJsonConfig(fastJsonConfig);
        HttpMessageConverter<?> converter = fastConverter;
        return new HttpMessageConverters(converter);
    }
    /*@Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder()
                .indentOutput(true)
                .dateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"))
                .modulesToInstall(new ParameterNamesModule());
        converters.add(new MappingJackson2HttpMessageConverter(builder.build()));
        converters.add(new MappingJackson2XmlHttpMessageConverter(Jackson2ObjectMapperBuilder.xml().build()));
    }*/
}
