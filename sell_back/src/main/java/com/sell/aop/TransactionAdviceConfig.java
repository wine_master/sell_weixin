package com.sell.aop;

import org.aspectj.lang.annotation.Aspect;
import org.springframework.aop.Advisor;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.interceptor.DefaultTransactionAttribute;
import org.springframework.transaction.interceptor.NameMatchTransactionAttributeSource;
import org.springframework.transaction.interceptor.TransactionInterceptor;

/**
 * @Company
 * @Classname TransactionAdviceConfig
 * @Description TODO 全局事务日志切面
 * @Author pn
 * Date 2021-09-07 16:56
 * Version 1.0
 */
@Aspect
@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE)
public class TransactionAdviceConfig {
 /*
 <!-- 配置切入点，在业务层中切入控制 -->
 <aop:pointcut expression="execution(* com.gok.service..*.* (..))"	id="pointCut" />
 <!-- "execution(修饰符匹配? 返回值类型匹配  操作类型匹配?  名称匹配(参数匹配)  抛出异常匹配)"：主要用于切入点的执行语法，各参数间用空格分隔开:
    expression="execution(* com.gok.service..*.*(..))" 解释：
         1、在上面的匹配中没有编写修饰符，如果要编写，可用public或private，？表示可以出现0次或1次
		 2、"*"：表示返回值，"*"支持所有类型的返回值
		 3、"com.gok.service.."：表示包，" .."表示包下任意子包
		 4、"*.*"：表示类.方法，"*.*"描述的是类中的所有方法
		 5、"(..)"：描述的是参数，"(..)"描述任意参数
 */


    //切入点
    private static final String AOP_POINTCUT_EXPRESSION = "execution(* com.gok.service..*.* (..))";

    //事务管理器
    @Autowired
    private PlatformTransactionManager transactionManager;

    @Bean
    public TransactionInterceptor txAdvice() {

        //事务传播属性配置
        DefaultTransactionAttribute txAttr_REQUIRED = new DefaultTransactionAttribute();
        txAttr_REQUIRED.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        DefaultTransactionAttribute txAttr_REQUIRED_READONLY = new DefaultTransactionAttribute();
        txAttr_REQUIRED_READONLY.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        txAttr_REQUIRED_READONLY.setReadOnly(true);
        NameMatchTransactionAttributeSource source = new NameMatchTransactionAttributeSource();

        /*所有的更新，启用独立事务*/
        source.addTransactionalMethod("insert*", txAttr_REQUIRED);
        source.addTransactionalMethod("add*", txAttr_REQUIRED);
        source.addTransactionalMethod("save*", txAttr_REQUIRED);
        source.addTransactionalMethod("delete*", txAttr_REQUIRED);
        source.addTransactionalMethod("remove*", txAttr_REQUIRED);
        source.addTransactionalMethod("drop*", txAttr_REQUIRED);
        source.addTransactionalMethod("rm*", txAttr_REQUIRED);
        source.addTransactionalMethod("update*", txAttr_REQUIRED);
        source.addTransactionalMethod("change*", txAttr_REQUIRED);
        source.addTransactionalMethod("edit*", txAttr_REQUIRED);
        source.addTransactionalMethod("exec*", txAttr_REQUIRED);
        source.addTransactionalMethod("set*", txAttr_REQUIRED);

        /*所有从库中取得数据的操作，只读事务*/
        source.addTransactionalMethod("get*", txAttr_REQUIRED_READONLY);
        source.addTransactionalMethod("query*", txAttr_REQUIRED_READONLY);
        source.addTransactionalMethod("find*", txAttr_REQUIRED_READONLY);
        source.addTransactionalMethod("list*", txAttr_REQUIRED_READONLY);
        source.addTransactionalMethod("count*", txAttr_REQUIRED_READONLY);
        source.addTransactionalMethod("is*", txAttr_REQUIRED_READONLY);
        source.addTransactionalMethod("load*", txAttr_REQUIRED_READONLY);
        source.addTransactionalMethod("search*", txAttr_REQUIRED_READONLY);
        source.addTransactionalMethod("select*", txAttr_REQUIRED_READONLY);
        source.addTransactionalMethod("*", txAttr_REQUIRED_READONLY);
        return new TransactionInterceptor(transactionManager, source);
    }

    @Bean
    public Advisor txAdviceAdvisor() {
        AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
        pointcut.setExpression(AOP_POINTCUT_EXPRESSION);
        /*<aop:advisor advice-ref="txAdvice" pointcut-ref="pointCut" />*/
        /*所有通知控制交由“事务通知处理完成”*/
        return new DefaultPointcutAdvisor(pointcut, txAdvice());
    }
}
