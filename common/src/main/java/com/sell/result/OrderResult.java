package com.sell.result;

import lombok.Data;

/**
 * @Company
 * @Classname
 * @Description TODO 生成订单返回结果类型
 * @Author wbs
 * Date 2021.8.16 10:42
 * Version 1.0
 */
@Data
public class OrderResult {
    private String messge;
    private String data;
}
