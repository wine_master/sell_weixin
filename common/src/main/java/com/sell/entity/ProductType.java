package com.sell.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductType implements Serializable {

    private Integer id;
    private String producttypename;
    private String producttypedesrc;
    private Date createtime;
    private Date updatetime;
}
