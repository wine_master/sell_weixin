package com.sell.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductInfo implements Serializable {

    private Integer id;  //主键
    private Integer producttypeid;  //类目id
    private String productname;//商品名称
    private String productdescribe;//商品描述
    private String producticon;//图标
    private double price;//价格
    private Integer stock;//库存
    private Date createtime;//创建时间
    private Date updatetime;//更新时间
    private Integer productstatus;//商品状态 未发布0 上架1 下架2

    private ProductType productType;
}
