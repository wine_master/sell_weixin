package com.sell.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TofUser implements Serializable {

    private String userid;
    private String username;
    private Integer sex;
    private Integer type;
    private String password;
    private String openid;
    private String nickname;
    private String phone;
    private String email;
    private String image;
    private String country;
    private String province;
    private String city;

}
