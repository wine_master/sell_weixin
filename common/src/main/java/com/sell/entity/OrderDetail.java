package com.sell.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDetail implements Serializable {

    private String orderdetailid;
    private String orderid;
    private Integer productid;
    private String productname;
    private String producticon;
    private Integer producttype;
    private Integer num;
    private double price;
    private Date creattime;
    private Date updatetime;
}
