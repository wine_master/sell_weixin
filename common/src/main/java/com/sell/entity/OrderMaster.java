package com.sell.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderMaster implements Serializable {

    private String orderid;//订单id
    private String openid;//微信openid
    private String buyername;//联系人姓名
    private String buyertelphone;//联系人电话
    private String buyeraddress;//联系人地址
    private double totalaccount; //总金额
    private Date createtime;//创建时间
    private Date updatetime;//更新时间
    private Integer orderstatus;//订单状态
    private Integer paystatus;//支付状态
    private String transactionid;
    private String refundid;
}
