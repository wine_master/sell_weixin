package com.sell.form;

import lombok.Data;

import java.util.Date;

/**
 * @Company
 * @Classname
 * @Description TODO 成功生成订单后的返回类型
 * @Author wbs
 * Date 2021.8.16 10:50
 * Version 1.0
 */
@Data
public class OrderOut {
    private String orderid;//单id
    private Double totalaccount;//总金额
    private Date createtime;//创建时间

    private String buyername;//联系人姓名
    private String buyertelphone;//联系人电话
    private String buyeraddress;//联系人地址

}
