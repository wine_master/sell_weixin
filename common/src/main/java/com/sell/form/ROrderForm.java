package com.sell.form;

import lombok.Data;

/**
 * @Company
 * @Classname
 * @Description TODO 买家端下单请求类型
 * @Author wbs
 * Date 2021.8.14 15:35
 * Version 1.0
 */
@Data
public class ROrderForm {
    private Integer productid;//商品id
    private String productname;//商品名称
    private String producticon;//商品图标
    private Integer producttype;//商品类目id
    private Integer num;//数量
    private Double price;//单价

    //微信id
    private String openid;
    //联系人姓名
    private String buyername;
    //联系人地址
    private String buyeraddress;
    //联系人电话
    private String buyertelphone;
}
