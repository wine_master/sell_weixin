package com.sell.form;

import lombok.Data;

import java.util.Date;

/**
 * @Company
 * @Classname
 * @Description TODO
 * @Author wbs
 * Date 2021.8.22 10:22
 * Version 1.0
 */
@Data
public class OrderForm2 {
    private String openid;//用户标识
    private String orderid;//单id
    /* @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
     @DateTimeFormat(pattern="yyyy-MM-dd")*/
    private Date createtime;//创建时间
    private String buyername;//联系人姓名
    private String buyertelphone;//联系人电话
    private Integer orderstatus;//订单状态
}
