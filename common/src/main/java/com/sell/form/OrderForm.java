package com.sell.form;

import com.sell.entity.OrderDetail;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Company
 * @Classname
 * @Description TODO
 * @Author pn
 * Date 2021.8.14 14:37
 * Version 1.0
 */
@Data
public class OrderForm implements Serializable {

    //购买清单
    private List<OrderDetail> lstOrderDetails = new ArrayList<>();


    ///////////以下是购买者相关信息/////////////
    //微信id
    private String openid;

    //联系人姓名
    private String buyername;
    //联系人地址
    private String buyeraddress;
    //联系人电话
    private String buyertelphone;
}