package com.sell.vo;

import lombok.Data;

/**
 * @Company
 * @Classname
 * @Description TODO 封装订单查询条件
 * @Author wbs
 * Date 2021.7.10 16:23
 * Version 1.0
 */
@Data
public class OrderMasterVo {
    //封装查询订单条件类型的对象
    private OrderMasterCondition orderMasterCondition;
}


