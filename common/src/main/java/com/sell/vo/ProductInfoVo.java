package com.sell.vo;

import lombok.Data;

/**
 * @Company
 * @Classname
 * @Description TODO 封装商品查询条件
 * @Author wbs
 * Date 2021.7.9 13:58
 * Version 1.0
 */
@Data
public class ProductInfoVo {
    private ProductInfoCondition productInfoCondition;
}
