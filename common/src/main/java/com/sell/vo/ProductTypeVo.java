package com.sell.vo;

import lombok.Data;

/**
 * @Company
 * @Classname
 * @Description TODO 封装类目查询条件
 * @Author wbs
 * Date 2021.7.9 12:27
 * Version 1.0
 */
@Data
public class ProductTypeVo {
    private ProductTypeCondition productTypeCondition;
}
